class Janken extends GamePvc(Game) {
  constructor() {
    super();
    this.listItemJanken = ["batu", "gunting", "kertas"];
  }

  suit(playerChoice, comChoice) {
    let result = {
      winner: "",
      message: "",
      hasWinner: true,
    };

    playerChoice = this.getRankFromItemSuit(playerChoice);
    comChoice = this.getRankFromItemSuit(comChoice);

    if (playerChoice === comChoice) {
      result.message = "DRAW";
      result.hasWinner = false;
    } else if (
      playerChoice - comChoice === 1 ||
      playerChoice - comChoice === -2
    ) {
      result.message = "PLAYER 1 WIN";
      result.winner = "player";
    } else {
      result.message = "COM WIN";
      result.winner = "com";
    }
    return result;
  }

  getRankFromItemSuit(userChoice) {
    if (userChoice === "batu") {
      return 0;
    } else if (userChoice === "kertas") {
      return 1;
    } else if (userChoice === "gunting") {
      return 2;
    } else {
      console.log("User choice not found");
    }
  }

  setItemJankenActive(userType, userChoice) {
    const itemActive = document.getElementsByClassName("active-" + userType);
    const itemSelected = document.getElementById(userChoice + "-" + userType);
    if (itemActive.length > 0) {
      itemActive[0].classList.remove("active-" + userType);
    }
    itemSelected.classList.add("active-" + userType);
  }

  resultGame(data) {
    const textVersus = document.getElementById("text-versus");
    const resultDraw = document.getElementById("result-draw");
    const resultWin = document.getElementById("result-winner");

    textVersus.classList.add("hidden");
    if (data.hasWinner) {
      resultWin.innerHTML = data.message;
      resultWin.classList.remove("hidden");
      resultDraw.classList.add("hidden");
    } else {
      resultWin.classList.add("hidden");
      resultDraw.classList.remove("hidden");
    }

    console.log("data game", data);
  }

  restartGame() {
    super.restartGame();
    const textVersus = document.getElementById("text-versus");
    const resultDraw = document.getElementById("result-draw");
    const resultWin = document.getElementById("result-winner");

    textVersus.classList.remove("hidden");
    resultDraw.classList.add("hidden");
    resultWin.classList.add("hidden");

    const itemActiveCom = document.getElementsByClassName("active-com");
    const itemActivePlayer = document.getElementsByClassName("active-player");
    if (itemActiveCom.length > 0) {
      itemActiveCom[0].classList.remove("active-com");
    }
    if (itemActivePlayer.length > 0) {
      itemActivePlayer[0].classList.remove("active-player");
    }
  }
}
