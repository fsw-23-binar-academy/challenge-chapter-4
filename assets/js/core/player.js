class PlayerUsers extends UserJanken {
  constructor(props) {
    super(props);
  }

  choose(itemJanken) {
    if (!this.isPlayed) {
      this.userChoice = itemJanken;
      this.setItemJankenActive(this.getTypeUser(), this.userChoice);
    }
    super.choose();
  }
}
